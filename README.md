<img src=".erb/img/app-preview.png" width="100%" />

<br>

<p>
  KahooTSE utilise <a href="https://github.com/electron-react-boilerplate/electron-react-boilerplate">Electron-react-boilerplate</a>, une base utilisant <a href="https://electron.atom.io/">Electron</a>, <a href="https://facebook.github.io/react/">React</a>, <a href="https://github.com/reactjs/react-router">React Router</a>, <a href="https://webpack.js.org/">Webpack</a> et <a href="https://www.npmjs.com/package/react-refresh">React Fast Refresh</a>.
</p>

<br>

## Installation

Cloner le dépôt et installez les dépendances:

```bash
git clone https://gitlab.com/flavien.fechino/kahootse.git
cd kahootse
npm install
cd model
install.bat
```
## Démarrage de l’application


```bash
npm start
```

note: un executable peut être créé avec `npm run package`, son fonctionnement n’est pas garanti par la localisation relative du modèle python.

## Livrables
Les fonctions lambdas sont disponibles dans le dossier FONCTIONS_LAMBDA
  
<a href="https://drive.google.com/file/d/17QLTLVVb9uJULQBMlzjyLOZTi_EbNhWx/view?usp=share_link">Ici tous les fichiers utilisés pour l’entraînement du modèle</a>

<a href="https://google.com/search?q=récursivité">Ce readme</a>