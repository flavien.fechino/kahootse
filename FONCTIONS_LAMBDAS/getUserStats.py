import boto3
import json

# Cette fonction va retourner les statistiques d'un utilisateur
# Requête POST avec comme paramètre dans le body:le pseudo et qui retourne les statistiques d'un utilisateur 
# Endpoint : https://m4h7b3qr62el5nlwkanjgk4hwe0flplo.lambda-url.us-east-1.on.aws/

def lambda_handler(event, context):
    # Get the request parameters
    data = event['body']
    dataParsed = json.loads(data)
    pseudo = dataParsed['Pseudo']

    # Connect to DynamoDB
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('Utilisateur')

    # Check if the user exists
    response = table.get_item(Key={'Pseudo': pseudo})

    if 'Item' not in response:
        return {
            'statusCode': 404,
            'body': 'User not found'
        }

    statObject = json.stringify({'Pseudo':response['Item']['Pseudo'],
                                'Naissance': response['Item']['Naissance'],
                                'Histoire':response['Item']['Histoire'],
                                'Geographie':response['Item']['Geographie'],
                                'Sport':response['Item']['Sport'],
                                'Culture Generale': response['Item']['Culture Generale'],
                                'Sciences':response['Item']['Sciences']
    })
    print(statObject)
    return {
        'statusCode': 200,
        'body': statObject
    }
