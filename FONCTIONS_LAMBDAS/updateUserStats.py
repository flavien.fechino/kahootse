import json
import boto3

# Cette fonction va permettre de modifier les statistiques d'un utilisateur
# Requête POST avec comme paramètre dans le body: (pseudo et les différentes catégories)
# Cette requête va modifier les statistiques d'un utilisateur dans la base de données
# Endpoint : https://bpnxgozcanhc6u3nivucdr2snm0fbydp.lambda-url.us-east-1.on.aws/


def lambda_handler(event, context):
    data = event['body']
    dataParsed = json.loads(data)
    # Récupération des données à update
    pseudo =dataParsed['Pseudo']
    bonnesReps = dataParsed['BonnesReps']
    Sport = bonnesReps['Sport']
    Sciences = bonnesReps['Sciences']
    Histoire = bonnesReps['Histoire']
    Geographie = bonnesReps['Geographie']
    Culture_Generale = bonnesReps['CultureG']
    
    # Connect to DynamoDB
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('Utilisateur')
    

    # Updating User Stats
    response = table.update_item(
        Key={'Pseudo': pseudo},
        UpdateExpression='SET Sport = :s, Sciences = :sci, Histoire = :h, Geographie = :g, CultureG = :c',
        ExpressionAttributeValues={
        ':s': Sport,
        ':sci': Sciences,
        ':h': Histoire,
        ':g': Geographie,
        ':c': Culture_Generale
        }
    )
    return {
        'statusCode': 200,
        'body': 'Les statistiques sont bien modifiées'
    }
