import boto3
import random

# Cette fonction va retourner 10 questions de la base de données choisies aléatoirement 
# Requête GET sans paramètre
# Endpoint : https://us55463czwcaqdj2p4r7c7g2440jupnu.lambda-url.us-east-1.on.aws/

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('Quizz_table')
    
    # Récupérer l'entièreté de la table
    response = table.scan()
    items = response['Items']
    
    # Boucle sur les pages de résultats si nécessaire
    while 'LastEvaluatedKey' in response:
        response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
        items += response['Items']
    
    # Choisir dix éléments au hasard
    random_items = random.sample(items, 10)
    
    return random_items
