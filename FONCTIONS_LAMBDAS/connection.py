import boto3
import json

def lambda_handler(event, context):
    try:
        # Get the request parameters
        print(event)
        data = event['body']
        dataParsed = json.loads(data)
        pseudo = dataParsed['Pseudo']
        password = dataParsed['Password']
    except KeyError as e:
        return {
            'statusCode': 400,
            'body': 'Bad Request: {} field is missing'.format(str(e))
        }

    try:
        # Connect to DynamoDB
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Utilisateur')

        # Check if the user exists
        response = table.get_item(Key={'Pseudo': pseudo})

        if 'Item' not in response:
            return {
                'statusCode': 404,
                'body': 'User not found'
            }

        if response['Item']['Password'] != password:
            return {
                'statusCode': 401,
                'body': 'Incorrect password'
            }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': 'Internal Server Error: {}'.format(str(e))
        }
    statObject ={'Pseudo':response['Item']['Pseudo'],
                                'Naissance': response['Item']['Naissance'],
                                'BonnesReps':{
                                'Histoire':response['Item']['Histoire'],
                                'Geographie':response['Item']['Geographie'],
                                'Sport':response['Item']['Sport'],
                                'CultureG': response['Item']['CultureG'],
                                'Sciences':response['Item']['Sciences']}
    }
    return {
        'statusCode': 200,
        'body': statObject
    }
