::Script BAT pour installer les dépendances ( optionnel )
@echo off
echo.
echo Installation de Numpy
py -m pip install numpy
echo.
echo Installation de Librosa
py -m pip install  librosa
echo.
echo Installation de TQDM
py -m pip install tqdm
echo.
echo Installation de Keras
py -m pip install keras
echo.
echo Installation de Tensorflow
py -m pip install tensorflow
python3 -m py -m pip install tensorflow
echo.
echo Installation de SkLearn
py -m pip install scikit-learn
echo.
echo Installation de MatPlotLib
py -m pip install matplotlib
echo.
echo. Installation de Pyaudio
python -m py -m pip install pyaudio
