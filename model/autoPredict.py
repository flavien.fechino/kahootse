#IMPORT
from keras.models import load_model
from PIL import Image
import numpy as np
import time
import pyaudio
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '5000' 
from tqdm import tqdm
import wave
import librosa
import librosa.display
import contextlib
import matplotlib.pyplot as plt
import sys
import warnings
warnings.filterwarnings("ignore")


#class Unbuffered(object):
   #def __init__(self, stream):
       #self.stream = stream
   #def write(self, data):
       #self.stream.write(data)
       #self.stream.flush()
   #def writelines(self, datas):
       #self.stream.writelines(datas)
       #self.stream.flush()
   #def __getattr__(self, attr):
       #return getattr(self.stream, attr)

#sys.stdout = Unbuffered(sys.stdout)

# On définit les specs de notre fichier audio de test
format = pyaudio.paInt16
channels = 2
rate = 44100
chunk = 1024
recordTime = 2
modelPath = 'C:/Users/FlFe/Documents/FISE3/ProjetBigData/KahooTSE/model/monModel.hdf5'
#soundPath = 'C:/Users/FlFe/Documents/FISE3/ProjetBigData/KahooTSE/src/recordings/rec.wav'
soundPath = 'C:/Users/FlFe/Documents/FISE3/ProjetBigData/KahooTSE/model/rec.mp3'
soundSize = (50,50)
labels = ['deux', 'non', 'oui','quatre','trois','un']
# Chargement modèle, timer et biblio audio
start = time.time()
model = load_model(modelPath)
sys.stdout.flush()
# On traite le fichier audio vers un tableau
data = []
print("sltpython")
y, sr = librosa.load(soundPath)
# Calcul du mel spectro selon notre audio
temp = librosa.feature.melspectrogram(y=y, sr=sr)
librosa.display.specshow(librosa.power_to_db(temp, ref=np.max))
# On convertit en image pour retaille la taille de l'image
canvas = plt.get_current_fig_manager().canvas
canvas.draw()
img = Image.frombytes('RGB', canvas.get_width_height(), canvas.tostring_rgb())
img = img.resize(size=soundSize)
img = np.asarray(img) / 255.
data.append(img)
plt.close()
data = np.asarray(data)
# On reshape notre tableau pour l'entrée de notre convNet
dimension = data[0].shape
data = data.astype(np.float32).reshape(data.shape[0], dimension[0],
dimension[1], dimension[2])
# On prédit
prediction = model.predict(data,verbose=0)
# On recupere le numero de label qui a la plus haute prediction
maxPredict = np.argmax(prediction)
# On recupere le mot correspondant à l'indice precedent
word = labels[maxPredict]
pred = prediction[0][maxPredict] * 100.
end = time.time()
# On affiche les prédictions
#print('RESULTAT : ' + word + ' : ' + "{0:.2f}%".format(pred))
print(word)