import React from 'react';
import '../styles/QuestionCard.css'
import {GiPartyPopper} from 'react-icons/gi'

const ResultCard = (props: any) => {
    return (
        <div className={"card " + props.className}>
            
            <p className='title-res'><GiPartyPopper/>Résultats<GiPartyPopper/></p>
            <p className='bonnesRep'>{props.nbCorrect + "/" + props.nbTotal}</p>
            <p>Vous avez fait mieux que {props.pourcentage}% des utilisateurs</p>
        </div>
    );
};

export default ResultCard;