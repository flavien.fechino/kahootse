import React from 'react';

import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper } from '@mui/material';

const MuiTable = (props: any) => {
    return (
        <div>
        <TableContainer component={Paper} sx={{maxHeight:"70vh", overflow:"auto"}}>
            <Table stickyHeader>
                <TableHead>
                    <TableRow>
                        {props.subject.map((subject: String)=>(
                            <TableCell align='center'>
                                {subject}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.subPercent.map((table: any)=>(
                        <TableRow>
                            {table.map((el: any)=>(
                                <TableCell align='center'>
                                    {el}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        </div>
    );
};

export default MuiTable