import React from 'react';
import '../styles/TitleBar.css'
const TitleBar = ({children}: any) => {
    return (
        <div id="mainWindow">
            <header>
                <div className="title">KahooTSE</div>
                <div className="blank"></div>
                <div className="wc-box">
                <div className="minimize" onClick={()=>window.electron.ipcRenderer.sendMessage("minimize",[])}></div>
                <div className="maximize"onClick={()=>window.electron.ipcRenderer.sendMessage("maximize",[])}></div>
                <div className="close"onClick={()=>window.electron.ipcRenderer.sendMessage("close",[])}></div>
                </div>
            </header>
            <div>
                {children}
            </div>
        </div>
    );
};

export default TitleBar;