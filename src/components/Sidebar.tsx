import React from 'react';
import {
    FaTh, FaUserAlt, FaRegChartBar, FaInfoCircle,FaHouseUser
}from 'react-icons/fa';
import {TbMicrophone2} from 'react-icons/tb'
import { NavLink } from 'react-router-dom';
import icon from '../../assets/Logo.png'
import { electron } from 'process';
const Sidebar = ({children}: any) =>{
    const menuItem=[
        {
            path:"/",
            name:"Home",
            icon:<FaTh/>
        },
        {
            path:"/user",
            name:"user",
            icon:<FaHouseUser />
        },
        {
            path:"/quizz",
            name:"Quizz",
            icon:<TbMicrophone2/>
        },
        {
            path:"/stats",
            name:"Your stats",
            icon:<FaRegChartBar/>
        },
        {
            path:"/about",
            name:"about",
            icon:<FaInfoCircle />
        },
    ]
    return (
        <div>
            <div className='container'>
                <div className="sidebar">
                    <div className='top'>
                        <img width="40" alt="icon" src={icon} draggable="false"></img>
                    </div>
                    {
                        menuItem.map((item,index)=>(
                            <NavLink to={item.path} key={index} className={({isActive}) => "link" +(isActive ? " active" : "")} end>
                                <div className="icon">{item.icon}</div>
                            </NavLink>
                        ))
                    }
                </div>
                <main>
                    {children}
                </main>
            </div>
        </div>
    )
}

export default Sidebar;