import React ,{useState} from 'react';
import '../styles/QuestionCard.css'
import { StyledButton } from 'pages/Quizz';

const QuestionCard = (props: any) => {
    const [bgColorList, setbgColorList] = useState(Array(props.responses.length).fill("#2c0735"));
    const [once,setOnce] = useState(false);
    if(props.answer>0 && !once){
        setOnce(true);
        props.setAnswer(-1);
        if(props.correct==props.answer){
            let newArr = [...bgColorList];
            newArr[props.answer-1] = "green";
            setbgColorList(newArr);
            props.plusone();
            console.log("Correct");
        }else{
            let newArr = [...bgColorList];
            newArr[props.answer-1] = "red";
            setbgColorList(newArr);
            props.pluszero();
            console.log("Incorrect");
        }
    }
    return (
        <div className={props.className}>
            <p className='question'>{props.question}</p>
            <section className="responses-container">
                {props.responses.map((res: string, index: number)=>(<StyledButton sx={{p:2, my:3, color:"#fff", bgcolor:bgColorList[index],':hover':{bgcolor:bgColorList[index]}}} key={index} className={"res res"+index} 
                onClick={()=>{
                        if(props.correct==index+1){
                            let newArr = [...bgColorList];
                            newArr[index] = "green";
                            setbgColorList(newArr);
                            props.plusone();
                            console.log("Correct");
                        }else{
                            let newArr = [...bgColorList];
                            newArr[index] = "red";
                            setbgColorList(newArr);
                            props.pluszero();
                            console.log("Incorrect");
                        }
                }}>{index+1 + ": " + res}</StyledButton>))}
            </section>
            <p className='questionIndex'>{props.nbCurrentQuestion+"/"+props.nbQuestion}</p>
        </div>
    );
};

export default QuestionCard;