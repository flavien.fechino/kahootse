import React,{useEffect, useState} from 'react';
import MuiTable from 'components/MuiTable';

const LeaderBoard = () => {
    const [userList, setUserList] = useState([]);
    console.log(userList);
    useEffect(()=>{
        fetch("https://3k3owjqzbhepxvxwfvhpq257fq0wlvvd.lambda-url.us-east-1.on.aws/").then((res)=>{
            res.json().then((data)=>setUserList(data))})
    },[]);
    
    function getSubjectList(){
        let rep = ["Pseudo"];
        if(userList.length>0){
        for (var key in userList[0].BonnesReps){
            rep.push(key)
        }
        }
        return rep
    }

    function getRows(){
        let rep = [];
        if(userList.length>0){
            userList.map((user)=>{
                let row = [user.Pseudo];
                for (var key in user.BonnesReps){
                    if(user.BonnesReps[key][1]!=0){
                        row.push((user.BonnesReps[key][0]/user.BonnesReps[key][1]*100).toFixed(2).toString() + "%")
                    }else{
                        row.push("0%")
                    }
                }
                rep.push(row);
            })
        }
        return rep;
    }

    return (
        <div>
           <h1 className='page-title'>Leaderboard</h1> 
           <MuiTable subject={getSubjectList()} subPercent={getRows()}></MuiTable>
        </div>
    );
};

export default LeaderBoard;