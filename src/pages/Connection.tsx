import React,{useState, useContext} from 'react';
import TextField from '@mui/material/TextField';
import { Dayjs } from 'dayjs';
import { Input,Button} from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { StyledButton } from 'pages/Quizz';
import { contentTracing } from 'electron';
import { UserInfoContext } from 'context/userInfoContext';
import MuiTable from '../components/MuiTable'
import localeFr from 'dayjs/locale/fr'

const Connection = (props: any) => {
    console.log(localeFr)
    const [userName,setUserName] = useState("");
    const [password,setPassword] = useState("");
    const [birthdate,setBirthdate] =React.useState<Dayjs | null>(null);
    const {userInfo, setUserInfo, isConnected, setIsConnected } = useContext(UserInfoContext)

    function getUserScore(){
        let perBonneReps=0;
        let totReps=0;
        for (var key in userInfo.BonnesReps){
            console.log(key)
            totReps+=userInfo.BonnesReps[key][1];
            perBonneReps+=userInfo.BonnesReps[key][0];
        }
        if(totReps>0){
            return(perBonneReps/totReps*100)
        }
        else{
            return(0)
        }
    }

    function getSubjectList(){
        let rep = [];
        for (var key in userInfo.BonnesReps){
            rep.push(key)
        }
        return rep
    }
    function getSubjectPercentage(){
        let res=[]
        for (var key in userInfo.BonnesReps){
            if(userInfo.BonnesReps[key][1]>0){
                res.push((userInfo.BonnesReps[key][0]/userInfo.BonnesReps[key][1]*100).toFixed(2).toString() + "%")
            }else{
                res.push("0%")
            }
        }
        return res
    }
    function handleConnection(event: any){
        event.preventDefault();
        (async () => {
            const rawResponse = await fetch('https://pglcauwcg5bztpjoxqioobpkzq0nibnx.lambda-url.us-east-1.on.aws/', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "Pseudo": userName,
                    "Password": password,
                })
            });
        const content = await rawResponse.json();
        console.log(content)
        console.log(rawResponse.ok)
        if(rawResponse.ok){
            setIsConnected(true);
            setUserInfo(content)
            getUserScore();
        }
        else{

        }
        })();
    }
    function handleRegistration(event:any){
        event.preventDefault();
        console.log(JSON.stringify({
                    "Pseudo": userName,
                    "Password": password,
                    "Naissance": birthdate?.format('YYYY-MM-DD')
                }));
        (async () => {
            const rawResponse = await fetch('https://jh2cn4cgyxye2bqqhs2n7xcsoi0enwxi.lambda-url.us-east-1.on.aws/', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "Pseudo": userName,
                    "Password": password,
                    "Naissance": birthdate?.format('YYYY-MM-DD')
                })
            });
        const content = await rawResponse.text();
        console.log(content);
        console.log(rawResponse.ok)
        })();
    }
    if(!(isConnected)){
        const fr = require('dayjs/locale/fr')
        return (
            <div>
                <h1 className='page-title'>User</h1>
                <div className='login-registration-columns'>
                    <form className='connection-form' onSubmit={handleConnection}>
                        <p>Login</p>
                        <Input type="text" placeholder='Pseudo' name="username" onInput={e=>setUserName(e.target.value)}   required></Input>
                        <Input type='password' placeholder='Mot de passe' name='password' onInput={e=>setPassword(e.target.value)} required></Input>
                        <StyledButton type='submit' id='submit'>Se connecter</StyledButton>
                    </form>
                    <div className='vertBar' style={{
                        border: '1px solid #fff',
                        borderRadius: "4px",
                        height: '50vh'
                    }}>
        </div>
                    <form className='connection-form' onSubmit={handleRegistration}>
                        <p>Register</p>
                        <Input type="text" placeholder='Pseudo' name="username" onInput={e=>setUserName(e.target.value)} required></Input>
                        <Input type='password' placeholder='Mot de passe' name='password' onInput={e=>setPassword(e.target.value)} required></Input>
                        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="fr">
                            <DatePicker
                                label="Date de naissance"
                                value={birthdate}
                                onChange={(newValue) => {
                                setBirthdate(newValue);
                                }}
                                renderInput={(params) => <TextField {...params} />}
                            />
                            </LocalizationProvider>
                        <StyledButton type='submit' id='submit'>Créer un nouveau compte</StyledButton>
                    </form>
                </div>
        </div>
        );
    }
    else{
        return(
            <div>
                <h1 className='page-title'>{userInfo.Pseudo}</h1>
                <p>{getUserScore().toFixed(2) + "% de bonnes réponses"}</p>
                <MuiTable subject={getSubjectList()} subPercent={[getSubjectPercentage()]}></MuiTable>
                <StyledButton onClick={()=>{setIsConnected(false);setUserInfo({})}}>Disconnect</StyledButton>
            </div>
        )
    }
};

export default Connection;