import React from 'react';
import icon from "../../assets/icon.svg"
import {HiOutlineMicrophone} from 'react-icons/hi'
const Home = () => {
    return (
        <div>
            <h1 className='page-title'>Bienvenue</h1>
            <h1>KahooTSE</h1>
            <div className="Hello">
                <HiOutlineMicrophone size="200px"></HiOutlineMicrophone>
            </div>
            <h2>Cliquez sur l’icone d’utilisateur pour créer un compte</h2>
            <h2>Ou bien sur le micro pour commencer à jouer</h2>
            <div className="Hello">
            </div>
        </div>
    );
};

export default Home;