import React from 'react';
import icon from '../../assets/Logo.png'
import icon2 from "../../assets/icon.svg"
import '../styles/About.css'
var pjson = require('../../package.json');

const About = () => {
    console.log(pjson.version)
    return (
        <div>
            <h1 className='page-title'>About</h1>
            <div className='presentation'>
                <section className='img-container'>

                <p>KahooTSE: A Multi-User voice controlled quizz app</p><br/>
                <img src={icon}></img>
                </section>
                <section className='img-container'>

                <p>Made from electron-react-boilerplate</p><br/>
                <img src={icon2}></img>
                </section>
            </div>
            <p className='version'>Version {pjson.version}</p>
            <p>Flavien Fechino | Emile Kleijn | Jérémy Lacroix | Hugo Luna | Aubin Plumail</p>
        </div>
    );
};

export default About;