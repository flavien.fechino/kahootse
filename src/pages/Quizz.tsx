import QuestionCard from 'components/QuestionCard';
import { useEffect, useState , useContext} from 'react';
import Button, {ButtonProps} from '@mui/material/Button';
import { alpha, styled } from '@mui/material/styles';
import ResultCard from 'components/ResultCard';
import { UserInfoContext } from 'context/userInfoContext';

const StyledButton = styled(Button)<ButtonProps>(({ theme }) => ({
    margin:theme.spacing(2),
    borderRadius:20,
    color:"#fff"
}));




const Quizz = (props: any) => {
    const [tenRes,setTenRes] = useState([]);
    const [nbCorrect,setnbCorrect] = useState(0);
    const [questionNb, setQuestionNb] = useState(0);
    const [prevurl,setPrevurl] = useState("");
    const [answer, setAnswer] = useState(-1);
    console.log("answer:",answer)
    const {userInfo, setUserInfo, isConnected, setIsConnected } = useContext(UserInfoContext)

    function twoSecRecord(){
            setTimeout(()=>{
                stopAndDownload();
            },2000)
    }
    function stopAndDownload(){
        window.electron.ipcRenderer.sendMessage('download',[]);
    }

    function updateUserInfoAWS(){
        console.log("updating", userInfo);
        (async () => {
            const rawResponse = await fetch('https://bpnxgozcanhc6u3nivucdr2snm0fbydp.lambda-url.us-east-1.on.aws/', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(userInfo),
            })})();
    }

    function addCorrect(){
        setnbCorrect(nbCorrect+1);
        if(isConnected){
            let arr = {...userInfo};
            arr["BonnesReps"][tenRes[questionNb]["Categorie"]][0]+=1
            arr["BonnesReps"][tenRes[questionNb]["Categorie"]][1]+=1
            setUserInfo(arr)
        }
    }

    function addIncorrect(){
        if(isConnected){
            let arr = {...userInfo};
            arr["BonnesReps"][tenRes[questionNb]["Categorie"]][1]+=1
            setUserInfo(arr)
        }
    }
    window.electron.ipcRenderer.removeAllListeners('answerDetected');
    window.electron.ipcRenderer.once('answerDetected',(arg)=>{
        console.log(arg);
        setAnswer(arg);
        setTimeout(()=>{setQuestionNb(questionNb+1);},3000);
    })
    useEffect(()=>{
        console.log({questionNb})
        if(questionNb<tenRes.length)
            twoSecRecord();
        if(questionNb<=tenRes.length && isConnected)
            updateUserInfoAWS();
    },[questionNb])
    useEffect(()=>{
        fetch("https://us55463czwcaqdj2p4r7c7g2440jupnu.lambda-url.us-east-1.on.aws/").then((res)=>{
        return(res.json())
        }).then((data)=>{
            setTenRes(data);
            console.log(data);
            //twoSecRecord();
            setTimeout(()=>{twoSecRecord()},5000); 
        })

        return()=>{
        }
    },[]);

    const renderSwitch = () =>{
        let cn = ""
        if(tenRes.length==questionNb && tenRes.length >0) {
                cn = "firstCard";}
        else{
                cn = "nextCard";
        }         
        if(tenRes.length-questionNb>=0){
        return <ResultCard className={cn} nbCorrect={nbCorrect} nbTotal={tenRes.length} pourcentage={0}></ResultCard>;
        }
    } 

    return (
        <div className='quizz-container'>
            <h1 className='page-title'>Kahoo<b>TSE</b></h1>
            <section className="quizzCards-container">
                {tenRes.map((el: any, index: number)=>{
                    var className=""
                    const diff = index-questionNb;
                    if(diff==-1){
                        className="previousCard"
                        return(<QuestionCard answer={-1} setAnswer={(el: any)=>setAnswer(el)} className={"card " + className } question={el.Question} responses={el.Reponses} correct={el.Correct} nbCurrentQuestion={index+1} nbQuestion={tenRes.length} plusone={()=>addCorrect()} pluszero={()=>addIncorrect()}></QuestionCard>)
                    }
                    if(diff==0){
                        className="firstCard"
                        return(<QuestionCard answer={answer} setAnswer={(el: any)=>setAnswer(el)} className={"card " + className } question={el.Question} responses={el.Reponses} correct={el.Correct} nbCurrentQuestion={index+1} nbQuestion={tenRes.length} plusone={()=>addCorrect()} pluszero={()=>addIncorrect()}></QuestionCard>)
                    }
                    if(diff==1){
                        className="nextCard"
                        return(<QuestionCard answer={-1} setAnswer={(el: any)=>setAnswer(el)} className={"card " + className } question={el.Question} responses={el.Reponses} correct={el.Correct} nbCurrentQuestion={index+1} nbQuestion={tenRes.length} plusone={()=>addCorrect()} pluszero={()=>addIncorrect()}></QuestionCard>)
                    }
                    if(diff>=-1 && diff<=1){
                    }
                })}
                {renderSwitch()}
            </section>
            <div className='navCards'>
                <StyledButton className='navCard' onClick={()=>{if(questionNb>0){setQuestionNb(questionNb-1)}}}>Previous</StyledButton>
                <StyledButton className="navCard" onClick={()=>{
                    if(questionNb<tenRes.length-1){
                        setQuestionNb(questionNb+1)
                        twoSecRecord()
                        //setTimeout(()=>{twoSecRecord()},5000); 
                    }
                    else if(questionNb<tenRes.length){
                        setQuestionNb(questionNb+1)
                        updateUserInfoAWS();
                    }
                    }}>Next</StyledButton>
            </div>
        </div>
    );
};

export {StyledButton};
export default Quizz;