import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import {useState, useContext} from 'react'
import icon from '../../assets/icon.svg';
import './App.css';
import TitleBar from 'components/TitleBar';
import Sidebar from 'components/Sidebar';
import Home from "pages/Home";
import About from 'pages/About';
import Stats from 'pages/Stats';
import Quizz from 'pages/Quizz';
import Connection from 'pages/Connection';
import UserInfoContextProvider from 'context/userInfoContext';
import LeaderBoard from 'pages/LeaderBoard';


export default function App() {
  return (
    <Router>
      <UserInfoContextProvider>
      <TitleBar>
        <Sidebar>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/quizz" element={<Quizz />} />
            <Route path="/about" element={<About />} />
            <Route path="/stats" element={<LeaderBoard/>} />
            <Route path="/user" element={<Connection />} />
          </Routes>
        </Sidebar>
      </TitleBar>
      </UserInfoContextProvider>
    </Router>
  );
}
