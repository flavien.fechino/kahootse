import { userInfo } from 'os'
import React, { createContext, useState } from 'react'
export const UserInfoContext = createContext()
const UserInfoContextProvider = (props) => {
    const [userInfo, setUserInfo] = useState({});
    const [isConnected,setIsConnected] = useState(false);
    return (
         <UserInfoContext.Provider 
            value={{
                userInfo,
                setUserInfo,
                isConnected,
                setIsConnected
             }}>
               {props.children}
         </UserInfoContext.Provider>
    )
}
export default UserInfoContextProvider